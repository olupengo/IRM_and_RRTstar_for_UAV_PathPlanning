% Example 1 of QCQP Problem solved by IRMA
% Minimize 2x_1x_2 + 4x_1x_3 + 6x_2x_3
% Subject to x_i^2=1, i=1,2,3,
%            x_1x_2-x_1x_3+1<=0

clear all
close all
% Define objective matrix
Q0 = [0 1 2;
	  1 0 3;
	  2 3 0];  
  
% Define ineqquality constriants  
Incon(1).Q = [ 0    0.5 -0.5;
               0.5  0    0;
              -0.5  0    0];  
Incon(1).c = 1; 

% Define eqquality constriants
Eqcon(1).Q = [1 0 0;
              0 0 0;
              0 0 0];
Eqcon(1).c = -1;

Eqcon(2).Q = [0 0 0;
              0 1 0;
              0 0 0];
Eqcon(2).c = -1;

Eqcon(3).Q = [0 0 0;
              0 0 0;
              0 0 1];
Eqcon(3).c = -1;

output = irma(Q0,Incon,Eqcon);
