% Example 2 of QCQP Problem solved by IRMA
%     &J = \min_{x\in\mathbb{R}^3} -2x_1 + x_2 - x_3 \nonumber\\
% s.t.&24 - 20x_1 + 9x_2 - 13x_3 + 4x^2_1 - 4x_1x_2 + 4x_1x_3 + 2x^2_2 - 2x_2x_3 + 2x^2_3 \ge 0 \nonumber\\
%     &x_1+x_2+x_3 \le 4, 3x_2+x_3 \le 6   \nonumber\\
%     &0 \le x_1 \le 1, 3 \le x_2, x_3 \le 0.\nonumber

clear all
close all
% Define objective matrix
Q0 = zeros(4);
Q0(1:3,4) = [-1;0.5;-0.5];
Q0(4,1:3) = [-1,0.5,-0.5];
  
% Define ineqquality constriants  
Incon(1).Q = -[ 4   -2     2     -10 ;
               -2    2    -1      4.5;
                2   -1     2     -6.5;
               -10   4.5  -6.5    0];  
Incon(1).c = -24; 

Incon(2).Q = zeros(4); 
Incon(2).Q(1:3,4) = 0.5;
Incon(2).Q(4,1:3) = 0.5;
Incon(2).c = -4;

Incon(3).Q = zeros(4); 
Incon(3).Q(1:3,4) = [0;1.5;0.5];
Incon(3).Q(4,1:3) = [0,1.5,0.5];
Incon(3).c = -6;

Incon(4).Q = zeros(4); 
Incon(4).Q(1,4) = 0.5;
Incon(4).Q(4,1) = 0.5;
Incon(4).c = -1;

Incon(5).Q = -Incon(4).Q;
Incon(5).c = 0;

Incon(6).Q = zeros(4); 
Incon(6).Q(2,4) = -0.5;
Incon(6).Q(4,2) = -0.5;
Incon(6).c = 3;

Incon(7).Q = zeros(4); 
Incon(7).Q(3,4) = 0.5;
Incon(7).Q(4,3) = 0.5;
Incon(7).c = 0;

% Define eqquality constriants
Eqcon(1).Q = zeros(4);
Eqcon(1).Q(4,4) = 1;
Eqcon(1).c = -1;



output = irma(Q0,Incon,Eqcon);