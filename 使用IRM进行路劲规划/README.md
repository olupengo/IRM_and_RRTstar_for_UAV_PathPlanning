# 使用IRM法求解一般QCQP问题

参考文献[1]使用秩极小化的迭代法(The iterative rank minimization algorithm, IRM)求解一般二次约束二次规划问题，并将该方法用到无人机路径规划中。本文中涉及的程序(irma.m等程序)源代码都可以在代码托管平台[码云](https://gitee.com/olupengo/IRM_and_RRTstar_for_UAV_PathPlanning/tree/master/%E4%BD%BF%E7%94%A8IRM%E8%BF%9B%E8%A1%8C%E8%B7%AF%E5%8A%B2%E8%A7%84%E5%88%92/%E7%90%86%E8%A7%A3IRMA)上找到，我的用户名：olupengo。
## QCQP问题
&emsp;&emsp;二次约束二次规划问题(Quadratical Constraint Quadratic Programming, QCQP)的目标函数，不等式约束都是二次的，具有如下形式
$$
\begin{aligned}
J=\mathrm{min}\quad \frac{1}{2}& x^{T}Q_{0}x+a_{0}^{T}x \\
\mathrm{s.t.}\quad \frac{1}{2} x^{T}Q_{j}x+a_{j}^{T}x &+c_{j}\leqslant0 \quad \mathrm{for} \space j=1,2,...,m \\
Ax&=b
\end{aligned} \tag{1}
$$
其中，$Q_{0},...,Q_{m}$是$n \times n$矩阵，$x \in \mathbb{R}^{n}$是优化变量。如果$Q_{0},...,Q_{m}$都是半正定矩阵，这一问题就是凸的。
&emsp;&emsp;文献[1]将路径规划问题转化为了非凸QCQP问题，形式如下
$$
\begin{aligned}
J=\mathrm{min}_{x} & x^{T}Q_{0}x+a_{0}^{T}x \\
\mathrm{s.t.}\quad x^{T}Q_{j}x+a_{j}^{T}x &+c_{j}\leqslant0 \quad \forall \space j=1,2,...,m \\
l_{x}\leqslant& x \leqslant u_{x}
\end{aligned}  \tag{2}
$$
其中，$x \in \mathbb{R}^{n}$是优化变量，$Q_{j}\in \mathbb{S^{n\times n}},j=1,2,...,m$是对称矩阵，$c_{j}\in \mathbb{R},j=1,...,m$，$a_{j}\in \mathbb{R}^{n},j=1,...,m$，$l_{x}\in \mathbb{R}^{n}$和$u_{x}\in \mathbb{R}^{n}$分别是$x$的下界和上界。
## IRM法介绍
&emsp;&emsp;如果遇到像问题(2)那样的非齐次QCQP问题，可以通过引入一个新变量$\alpha \in \mathbb{R}$和一个新约束$\alpha^{2}=1$，将非齐次QCQP问题转化为齐次QCQP问题。例如问题(2)可以转化为如下式(3)的齐次形式
$$
\begin{aligned}
J=\mathrm{min}[x^{T}\quad \alpha]
\begin{bmatrix}
   Q_{0} & a_{0}/2 \\
   a_{0}^{T}/2 & 0
\end{bmatrix}&
\begin{bmatrix}
   x\\
   \alpha
\end{bmatrix} \\
\mathrm{s.t.}\quad
[x^{T}\quad \alpha]
\begin{bmatrix}
   Q_{j} & a_{j}/2 \\
   a_{j}^{T}/2 & 0
\end{bmatrix}&
\begin{bmatrix}
   x\\
   \alpha
\end{bmatrix}+c_{j}\leqslant 0 \\
l_{x}\leqslant x \leqslant& u_{x} \\
\alpha^{2}=1&
\end{aligned} \tag{3}
$$

$$
\begin{aligned}
[x^{T}\quad \alpha]
\begin{bmatrix}
   Q_{j} & a_{j}/2 \\
   a_{j}^{T}/2 & 0
\end{bmatrix}&
\begin{bmatrix}
   x\\
   \alpha
\end{bmatrix}+c_{j}\leqslant 0 \\
\end{aligned} \tag{4}
$$由(4)式可得
$$
\begin{aligned}
x^{T}Q_{j}x+\alpha a_{j}^{T}x+c_{j}\leqslant0
\end{aligned} \tag{5}
$$
将(5)式两边同时除以$\alpha^{2}$可得
$$
\begin{aligned}
(x/\alpha)^{T}Q_{j}(x/\alpha)+a_{j}^{T}(x/\alpha) +c_{j}\leqslant0
\end{aligned} \tag{6}
$$对比(2)式和(6)式可知，如果通过求解问题(3)得到最优解$(x^{*},\alpha^{*})$，那么$x^{*}/\alpha^{*}$一定是问题(2)的最优解。
&emsp;&emsp;齐次QCQP问题可以写成以下更简洁的形式
$$
\begin{aligned}
J=\mathrm{min} &\space x^{T}Q_{0}x\\
\mathrm{s.t.} x^{T}Q_{j}x \leqslant c_{j},& \space \forall j=1,...,m \\
\end{aligned} \tag{7}
$$
&emsp;&emsp;引入秩1半正定矩阵$X=xx^{T}$，将齐次QCQP问题(7)松弛为式(8)所示半定规划问题(semidefinite programming，SDP)
$$\begin{aligned}
J=\mathrm{min}_{X} & \left \langle X,Q_{0} \right \rangle \\
\mathrm{s.t.} \left \langle X,Q_{j} \right \rangle \leqslant c_{j}, &\space \forall j=1,...,m \\
X \succcurlyeq & 0 \\
\end{aligned} \tag{8}
$$
&emsp;&emsp;此时的问题(8)不能等价于原本的问题，因为问题(8)中的$X$不满足秩1约束。已知，当$X$是非零正定矩阵时，$X$是秩1矩阵的充要条件为$rI_{n-1}-V^{T} XV \succcurlyeq 0$，其中$V \in \mathbb{R}^{n \times (n-1)}$是$X$的$n-1$个较小特征值所对应的特征向量组成的矩阵，$r$是趋于0的正数。IRM算法通过迭代的方法，逐渐减小$X$的秩。所以可以把问题(8)转化为下面的凸优化问题
$$\begin{aligned}
J=\mathrm{min}_{X_{k},r_{k}} & \left \langle  X_{k}, Q_{0} \right \rangle + w^{k}r_{k} \\
\mathrm{s.t.} \left \langle X,Q_{j} \right \rangle \leqslant c_{j}, &\space \forall j=1,...,m \\
X_{k} \succcurlyeq & 0 \\
r_{k}I_{n-1}-V_{k-1}^{T} & X_{k}V_{k-1} \succcurlyeq 0 \\
\end{aligned} \tag{9}$$
其中，$w>1$为$r_k$的权重系数。
&emsp;&emsp;IRM法通过求解上面的SDP问题(8)获得$X_{0}$，通过$X_{0}$求得$V_{0}$，然后通过求解问题(9)获得$X_{k}$，求得$V_{k}$，不断迭代直到$r_{k}$足够小。IRM法的详细推导过程见参考文献[2]，如果只是想使用这个IRM算法，可以直接看下面的例子。

## IRM法使用实例
1. 求单位圆上$x$最小的点的坐标。

$$
\begin{aligned}
J=&\mathrm{min}(x) \\
\mathrm{s.t.}\quad	x^2&+y^2=1
\end{aligned} \tag{10}
$$使用`irma.m`子函数求解问题(10)的MATLAB程序`untitled1.m`已上传至码云。

2. 求单位圆上$x^{2}$最小的点的坐标，注意：代价函数和约束都是二次就不要加$\alpha^2=1$这个约束了。

$$
\begin{aligned}
J=&\mathrm{min}(x^{2}) \\
\mathrm{s.t.}\quad &x^2+y^2=1
\end{aligned} \tag{11}
$$使用`irma.m`子函数求解问题(11)的MATLAB程序`untitled2.m`以上传至码云。
3. 求单位圆上距离$(1,1)$最近的点的坐标。

$$
\begin{aligned}
J = \mathrm{min}((x-1&)^{2}+(y-1)^{2}) \\
\mathrm{s.t.}\quad	x^2+&y^2=1
\end{aligned} \tag{12}
$$
使用`irma.m`求解问题(12)的源代码`untitled3.m`以上传至码云。

4. 求到两个固定端点$(0,0)$和$(10,10)$距离相等，并且到两个固定端点距离相等的点的坐标。

$$
\begin{aligned}
J=\mathrm{min}( (x_2-x_1&)^2+(y_2-y_1)^2 ) \\
\mathrm{s.t.}\quad (x_2-x_1)^2+(y_2-y_1)^2 &= (x_3-x_2)^2+(y_3-y_2)^2 \\
0 \leqslant x_1 &\leqslant 0 \\
0 \leqslant y_1 &\leqslant 0 \\
0 \leqslant x_2 &\leqslant 10 \\
0 \leqslant y_2 &\leqslant 12 \\
10 \leqslant x_3 &\leqslant 10 \\
10 \leqslant y_3 &  \leqslant 10 \\
\end{aligned} \tag{13}
$$
使用`irma.m`求解问题(13)的源代码`untitled4.m`以上传至码云。

## 参考
[1] Sun C ,  Liu Y C ,  Dai R , et al. Two Approaches for Path Planning of Unmanned Aerial Vehicles with Avoidance Zones[J]. Journal of Guidance Control & Dynamics, 2017, 40(8).
[2] Sun C ,  Dai R . An iterative approach to Rank Minimization Problems[C]// Decision & Control. IEEE, 2016.