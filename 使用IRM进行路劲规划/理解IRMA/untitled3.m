% 求解以下优化问题(单位圆上到（1,1）最近的点的坐标)
% J=min( (x-1)^2+(y-1)^2 )
% x^2+y^2=1
% 注意：如果代价函数和约束都是二次就不要加α^2=1这个约束了！！！
% 程序运行正常！
% 日期：2018-11-14

clear
Q0 = zeros(3,3);  %min( x^2+y^2-2x-2y )
Q0(1,1)=1;
Q0(2,2)=1;
Q0(3,1:2)=[-1 -1];
Q0(1:2,3)=[-1 -1];

Eqcon(1).Q=zeros(3,3);  %x^2+y^2=1
Eqcon(1).Q(1,1)=1;
Eqcon(1).Q(2,2)=1;
Eqcon(1).c=-1;

Eqcon(2).Q=zeros(3,3);  %α^2=1
Eqcon(2).Q(3,3)=1;
Eqcon(2).c=-1;

% Incon(1).Q=zeros(3,3);  %y>=0
% Incon(1).Q(3,2)=-0.5;
% Incon(1).Q(2,3)=-0.5;
% Incon(1).c=0;
Incon=[];

output=irma(Q0,Incon,Eqcon);
output.x