 % 求解以下优化问题（单位圆上x最小的点）
% J=min(x)
% x^2+y^2=1
% y>=0(可不用此约束)

clear
Q0 = zeros(3,3);
Q0(3,1)=0.5;
Q0(1,3)=0.5;

Eqcon(1).Q=zeros(3,3);  %x^2+y^2=1
Eqcon(1).Q(1,1)=1;
Eqcon(1).Q(2,2)=1;
Eqcon(1).c=-1;

Eqcon(2).Q=zeros(3,3);  %α^2=1
Eqcon(2).Q(3,3)=1;
Eqcon(2).c=-1;

% Incon(1).Q=zeros(3,3);  %y>=0
% Incon(1).Q(3,2)=-0.5;
% Incon(1).Q(2,3)=-0.5;
% Incon(1).c=0;
Incon=[];

output=irma(Q0,Incon,Eqcon);