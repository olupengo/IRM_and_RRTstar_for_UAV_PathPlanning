# irma.m函数使用实例

该irma.m函数可以用来求解一般二次约束二次规划问题。下面给出了几个实例。
1. 求单位圆上$x$最小的点的坐标。
$$
\begin{aligned}
J=&\mathrm{min}(x)\\
\mathrm{s.t.}\quad x^2&+y^2=1
\end{aligned}
$$
使用`irma.m`子函数求解该问题的MATLAB程序是`untitled1.m`

2. 求单位圆上$x^{2}$最小的点的坐标，注意：代价函数和约束都是二次就不要加α^2=1这个约束了。
$$
\begin{aligned}
J=&\mathrm{min}(x^{2}) \\
\mathrm{s.t.}\quad	&x^2+y^2=1
\end{aligned}
$$
使用`irma.m`子函数求解该问题的MATLAB程序是`untitled2.m`

3. 求单位圆上距离$(1,1)$最近的点的坐标。
$$
\begin{aligned}
J = \mathrm{min}((x-1&)^{2}+(y-1)^{2}) \\
\mathrm{s.t.}\quad	x^2+&y^2=1
\end{aligned}
$$
使用`irma.m`求解问题3的源代码是`untitled3.m`。

4. 求到两个固定端点$(0,0)$和$(10,10)$距离相等，并且到两个固定端点距离相等的点的坐标。
$$
\begin{aligned}
J=\mathrm{min}( (x_2-x_1&)^2+(y_2-y_1)^2 ) \\
\mathrm{s.t.}\quad (x_2-x_1)^2+(y_2-y_1)^2 &= (x_3-x_2)^2+(y_3-y_2)^2 \\
0 \leqslant x_1 &\leqslant 0 \\
0 \leqslant y_1 &\leqslant 0 \\
0 \leqslant x_2 &\leqslant 10 \\
0 \leqslant y_2 &\leqslant 12 \\
10 \leqslant x_3 &\leqslant 10 \\
10 \leqslant y_3 &  \leqslant 10 \\
\end{aligned}
$$
使用`irma.m`求解问题4的源代码是`untitled4.m`。