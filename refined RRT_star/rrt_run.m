function []=rrt_run

p_start = [0;0];    %起点位置
p_goal = [10;10];   %目标位置

param.obstacles =[5,5,5,5;];  %一行代表一个椭圆
param.searchFeild = [-2,10.5,0,12];   %搜索区域
param.threshold = 0.3;   %d_{min}
param.theta = 5*pi/6;    %论文中theta取150度
param.resolution = 10;   %决定在路径上多远取一个点，以判断路径是否避开障碍
param.maxNodes = 800;
param.step_size = 6;
param.neighbourhood = 1; %设置neighbourhood的半径r
param.random_seed = 20;
param.u_max = 0.2;       %

result = RRTstar(param, p_start, p_goal)
end